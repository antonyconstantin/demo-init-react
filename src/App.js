import logo from './logo.svg';
import './App.css';
import { Card } from './card/Card';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div class="game">
          <Card value={0} color="coeur" />
          <Card value={10} color="trefle" />
          <Card value={7} color="pique" />
          <Card value={8} color="carreau" />
          <Card value={5} color="carreau" />
          <Card value={4} color="coeur" />
        </div>
      </header>
    </div>
  );
}

export default App;
