import React, { useState } from 'react';
import './Card.css';
export function Card(props) {
  const [switcheur, setSwitch] = useState("active");

  function handlerClick(e){
    actionA();
    actionB();

  }
  return (
    <div className={`Card ${switcheur}`} onClick={handlerClick}>
      <div className={props.color}>
        <div class="recto">{props.value}</div>
        <div class="verso"></div>
      </div>
    </div>
  );
}